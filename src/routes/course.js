const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const course = require('../models/course');

//Course 
router.post('/',courseController.postCourse);

router.get('/:id',courseController.getcourseById);

router.get('/',courseController.getAllcourse);

router.put('/update/:id',courseController.updateCourse);

router.delete('/:id',courseController.delById);

module.exports = router;