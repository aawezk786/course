const mongoose = require('mongoose');



//Course Schema
const courseSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title : {
        type : String
    },
    description : {
        type : String
    },
    content : {
        type : Array
    },
    requirements : {
        type : String
    },
    createdBy : {
        type : String
    },
    purpose : {
        type : String
    },
    video : {
        type : String
    },
    price : {
        type : Number
    },
    discount : {
        type : Number
    }

},{timestamps:true});


module.exports = mongoose.model('Course', courseSchema);