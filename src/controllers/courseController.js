const createError = require('http-errors');
const Course = require('../models/course');
const mongoose = require('mongoose');

exports.postCourse = (req,res,next) =>{
    if (!req.file && !req.body) {
        const error = new Error("Body is Empty");
        error.statusCode = 422;
        return next(error);
    }
    const course = new Course({
        _id : new mongoose.Types.ObjectId(),
        title : req.body.title,
        description : req.body.description,
        content : req.body.content,
        requirements : req.body.requirements,
        createdBy : req.body.createdBy,
        purpose : req.body.purpose,
        video : req.body.video,
        price : req.body.price,
        discount : req.body.discount
    })

    course.save()
    .then(data =>{
        res.status(201).json({
            statusCode : 201,
            message : "Created Successfully",
            data : data
        });
    }).catch(err=>{
        next(err)
    });
}

exports.getcourseById = async (req,res,next) =>{
    try {
        const course = await Course.findById(req.params.id);
        res.status(200).json({
            statusCode: 200,
            message: "success",
            data: course
        });
    } catch (err) {
        next(err);
    }
}

exports.getAllcourse = async (req,res,next) =>{
    try {
        const course = await Course.find({});
        res.status(200).json({
            statusCode: 200,
            message: "success",
            data: course
        });
    } catch (err) {
        next(err);
    }
}

exports.updateCourse = (req,res,next) =>{
    const id = req.params.id;

    Course.updateOne({_id: id},{
        title : req.body.title,
        description : req.body.description,
        content : req.body.content,
        requirements : req.body.requirements,
        createdBy : req.body.createdBy,
        purpose : req.body.purpose,
        video : req.body.video,
        price : req.body.price,
        discount : req.body.discount
    }).then(result =>{
        res.status(200).json({
            statusCode: 200,
            message: "success",
            data: result
        });
    }).catch(err=>{
        next(err)
    });
}


exports.delById = async (req,res,next)=>{
    try {
        const course = await Course.findById(req.params.id);
        const deletedCourse = await Course.findByIdAndRemove(req.params.id);

        if(!course)
        throw next(createError.NotAcceptable('Check ID'));

        res.status(200).json({
            statusCode : 200,
            message: "Course Deleted Successfully",
            data: deletedCourse
        });
    } catch (err) {
        next(err);
    }
}
